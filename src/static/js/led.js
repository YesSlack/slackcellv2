
var led_bg_color = document.querySelector('#led_bg_color');
var led_color = document.querySelector('#led_color');
var led_mode = document.getElementById('led_mode');

let led_color_picker = new Picker({
          parent: led_color,
          popup: 'top',
          color: 'blue',
          alpha: false
    }
);

// You can do what you want with the chosen color using two callbacks: onChange and onDone.
led_color_picker.onChange = function(color) {
    led_color.style.background = color.rgbaString;
};

let led_bg_color_picker = new Picker({
          parent: led_bg_color,
          popup: 'top',
          color: 'black',
          alpha: false
    }
);

// You can do what you want with the chosen color using two callbacks: onChange and onDone.
led_bg_color_picker.onChange = function(color) {
    led_bg_color.style.background = color.rgbaString;
};

function display_led_mode_options(){
    let classes = led_mode.options
    for (let i = 0; i < classes.length; i++){
        for (let el of document.querySelectorAll('.led_' + classes[i].label.toLowerCase())) el.style.display = 'none';
    }
    for (let el of document.querySelectorAll('.led_' + led_mode.value.toLowerCase())) el.style.display = 'flex';
}
function get_input_number(id, default_value){
    let value = parseInt(document.getElementById(id).value);
            if(value){
                return value
            }else{
                return default_value
            }
}

function get_slider(id){
    return document.getElementById(id).value*1
}

function switch_leds(){
    let options = document.getElementById("leds_on").checked
    get_api("/led_switch/" + JSON.stringify(options)).then(val => {console.log(val)});
}

function number_leds(){
    let n = get_input_number("number_leds", 10)
    get_api("/number_leds/" + n).then(val => {console.log(val)});
}

function set_led_mode(){
    let mode = led_mode.value.toLowerCase();
    let options = {};
    switch (mode) {
        case "bar":
            options['n_centers'] = get_input_number('bar_n_centers', 0)
            options['color'] = led_color.style.backgroundColor.match(/([0-9]+\.?[0-9]*)/g).map(x=>+x)
            options['background_color'] = led_bg_color.style.backgroundColor.match(/([0-9]+\.?[0-9]*)/g).map(x=>+x)
            break;
        case "brightness":
            options['b_min'] = get_slider('brightness_min')
            options['b_max'] = get_slider('brightness_max')
            options['color'] = led_color.style.backgroundColor.match(/([0-9]+\.?[0-9]*)/g).map(x=>+x)
            break;
        case "hue":
            options['h_min'] = get_slider('hue_min')
            options['h_max'] = get_slider('hue_max')
            break;
        case "rainbow":
            options['n_rainbows'] = get_input_number('n_rainbows', 1)
            options['min_speed'] = get_input_number('min_speed', 3)
            options['max_speed'] = get_input_number('max_speed', 10)
            break;
    }
    get_api("/led_mode/" + mode + "/" + JSON.stringify(options)).then(val => {console.log(val)});
}

display_led_mode_options()