import platform
import colorsys
import numpy as np

from rpi_ws281x import PixelStrip, Color
PI = platform.uname()[4].startswith("arm")


class Led:
    LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
    LED_DMA = 10          # DMA channel to use for generating signal (try 10)
    LED_INVERT = False    # True to invert the signal (when using NPN transistor level shift)
    LED_CHANNEL = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

    def __init__(self, led_count=16, led_mode='rainbow', led_brightness=100):
        # led_pin:                GPIO pin connected to the pixels (18 uses PWM!).
        # LED_PIN = 10        # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
        self.count = led_count  # theoretical limit with PWM: 2700 LEDs https://github.com/rpi-ws281x/rpi-ws281x-python/tree/master/library
        if not PI:
            self.count = 158
        self.brightness = led_brightness  # Set to 0 for darkest and 255 for brightest
        self.strip = PixelStrip(self.count, 18, self.LED_FREQ_HZ, self.LED_DMA, self.LED_INVERT,
                                self.brightness, self.LED_CHANNEL)
        self.strip.begin()
        self.mode = led_mode
        self.options = {}
        self.counter = 0
        self.current_draw = 0
        self.on = False

    def set_count(self, led_count):
        self.set_all_pixels_color("black")
        self.strip.show()
        self.count = led_count
        self.strip = PixelStrip(led_count, 18, self.LED_FREQ_HZ, self.LED_DMA, self.LED_INVERT,
                                self.brightness, self.LED_CHANNEL)
        self.strip.begin()

    def set_mode(self, mode, options):
        on = self.on
        self.on = False
        self.mode = mode
        self.options = options
        self.on = on

    def display(self, lc):
        if self.on:
            if self.mode == "bar":
                self.mode_bar(lc, **self.options)

            if self.mode == "hue":
                self.mode_hue(lc, **self.options)

            if self.mode == "rainbow":
                self.mode_rainbow(lc, **self.options)

            if self.mode == "brightness":
                self.mode_brightness(lc, **self.options)

            color_sum = 0
            for pixel in range(self.strip.numPixels()):
                color = self.strip.getPixelColorRGB(pixel)
                color_sum = color_sum + color.r + color.g + color.b
            self.current_draw = color_sum / 255 * 20 / 1000
            self.strip.show()
            if PI:
                return None
            else:
                return ["#" + format(pixel, 'x').zfill(6) for pixel in self.strip.getPixels()]

    def mode_hue(self, lc, h_min=0.1, h_max=1):
        val = mapp(lc.force, lc.min_force, lc.max_force, h_min, h_max)
        color = hsv2rgb(val, 1, 0.5)
        self.set_all_pixels_color(color)

    def mode_brightness(self, lc, b_min=50, b_max=255, color=[0, 0, 255]):
        val = round(mapp(lc.force, lc.min_force, lc.max_force, b_min, b_max))
        self.set_all_pixels_color(Color(*color))
        self.strip.setBrightness(val)

    def mode_bar(self, lc, n_centers=4, color=[0, 0, 255], background_color=[127, 0, 0]):
        centers = [round(c) for c in np.linspace(0, self.count, 2 * n_centers + 1)]
        centers = centers[1::2]
        # leds lighting up from end to beginning from strip
        if n_centers == 0:
            centers = [self.count]

        if n_centers == 0:
            lighted_pixels = round(mapp(lc.force, lc.min_force, lc.max_force, 0, 1) * self.count)
        else:
            lighted_pixels = round(mapp(lc.force, lc.min_force, lc.max_force, 0, 0.5 / n_centers) * self.count)
        # reset all pixels
        self.set_all_pixels_color(Color(*background_color))
        for center in centers:
            for i in range(max(center - lighted_pixels, 0), min(center + lighted_pixels, self.count)):
                self.strip.setPixelColor(i, Color(*color))

    def mode_rainbow(self, lc, n_rainbows=2, min_speed=3, max_speed=10):
        """Draw rainbow that uniformly distributes itself across all pixels."""
        speed = round(mapp(lc.force, lc.min_force, lc.max_force, min_speed, max_speed))
        for i in range(self.strip.numPixels()):
            self.strip.setPixelColor(i, wheel(
                (int(i * 256 / self.strip.numPixels() * n_rainbows) + self.counter) & 255))
        self.strip.show()
        self.counter += speed

    def set_all_pixels_color(self, color):
        if color == "black":
            color = Color(0, 0, 0)
        if self.strip.started:
            for i in range(self.count):
                self.strip.setPixelColor(i, color)

    def switch_off(self):
        self.set_all_pixels_color("black")
        self.strip.show()
        self.current_draw = 0
        self.on = False

    def switch_on(self):
        self.on = True


def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)


def mapp(x: float, in_min: float, in_max: float, out_min: float, out_max: float) -> float:
    try:
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min
    except ZeroDivisionError:
        return out_min


def hsv2rgb(h, s, v):
    c = tuple(round(i * 255) for i in colorsys.hsv_to_rgb(h, s, v))
    return Color(c[0], c[1], c[2])
